package fileStorage

import (
	"hash/crc32"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type FilePlacementStrategy interface {
	AllocateForFile(name string) string
	AllocateForFileShort(name string) string
	AllocateForPrefixed(prefix string) string
}

type DateBasedFilePlacer struct {
	RootDir string
}

func (p DateBasedFilePlacer) AllocateForFile(name string) string {
	return p.AllocateForFileReal(name, true)
}

func (p DateBasedFilePlacer) AllocateForFileShort(name string) string {
	return p.AllocateForFileReal(name, false)
}

func (p DateBasedFilePlacer) AllocateForFileReal(name string, withRoot bool) string {
	now := time.Now()

	directory := strconv.FormatInt(int64(now.Year()), 36) +
		strconv.FormatInt(int64(now.Month()), 36)

	dirPath := p.RootDir + directory

	nameHash := now.Unix() | int64(crc32.Checksum([]byte(name), crc32.IEEETable))

	if os.MkdirAll(dirPath, 0777) != nil {
		panic("Unable to create directory" + directory)
	}

	if withRoot {
		return strings.Join([]string{
			p.RootDir,
			directory,
			"/",
			strconv.FormatInt(nameHash, 36),
			filepath.Ext(name),
		}, "")

	} else {
		return strings.Join([]string{
			directory,
			"/",
			strconv.FormatInt(nameHash, 36),
			filepath.Ext(name),
		}, "")

	}
}

func (p DateBasedFilePlacer) AllocateForPrefix(prefix string) string {
	return "nil"
}
